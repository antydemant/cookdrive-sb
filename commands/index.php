<?php
//parser library
require(__DIR__ . '/../vendor/autoload.php');
use Sunra\PhpSimple\HtmlDomParser;

$filename = 'runtime/output.json';

//create or open file if exists
    $file = fopen($filename, 'w');

//init service link
$SERVICE='http://cookdrive.com.ua';

//open service's page
$html = HtmlDomParser::file_get_html($SERVICE) or die("Wrong service's URL.");

$productsArray = array();

foreach ($html->find('ul', 1)->find('li') as $category){
    $link = $category->find('a', 0)->href;

    $categoryName = trim($category->find('a', 0)->plaintext);

    //open category's page
    $html = HtmlDomParser::file_get_html($SERVICE . $link) or die ("Category's url has changed.");

    $items = array();

    //find product
    foreach($html->find('.catalog__item') as $element) {
        if ($product_short_link = $element->find('a', 0)){
            $productLink = $SERVICE . $product_short_link->href;
        }

        $newprice = 0;

        $newprice_element = $element->find('.catalog__item-price-value',0);

        if($newprice_element->hasChildNodes()) {
            $newprice = trim(substr(strstr(trim($newprice_element->children(0)->parentNode()->innertext()), '</del>'), 6));
        }

        $sub = $element->parentNode()->parentNode()->previousSibling()->plaintext;
        $productName = trim($element->find('.catalog__item-title', 0)->find('a', 0)->plaintext);
        $subcategory = trim($sub);
        $weight = trim($element->find('.catalog__item-feature', 0)->plaintext);
        $description = trim($element->find('.catalog__item-desc p', 0)->plaintext);

        if($newprice) {
            $price = (double) $newprice;
        } else {
            $price = (double) $element->find('.catalog__item-price-value', 0)->innertext();
        }
        $photo_url = trim($product_short_link->find('img', 0)->src);

        if (!empty($productName)) {
            $items[] = array(
                'product_name' => !empty($productName) ? $productName : '',
                'subcategory' => $subcategory,
                'weight' => !empty($weight) ? $weight : '',
                'description' => $description,
                'price' => !empty($price) ? $price : '',
                'photo_url' => !empty($photo_url)? $SERVICE . $photo_url : '',
                'link' => $productLink,
            );
        }
    }

    $productsArray[$categoryName] = $items;
}

fwrite($file, json_encode($productsArray, JSON_UNESCAPED_UNICODE));
fclose($file);
?>